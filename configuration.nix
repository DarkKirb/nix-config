# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
    imports = [ # Include the results of the hardware scan.
        ./hardware-configuration.nix
    ];

# Use the systemd-boot EFI boot loader.
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
    boot.supportedFilesystems = ["zfs"];
    boot.zfs.enableUnstable = true;
    boot.tmpOnTmpfs = true;
    boot.kernelPackages = pkgs.linuxPackages_4_18;

    networking = {
        hostName = "star-dream";
        hostId = "c5549e81";
        defaultGateway = {
            address = "192.168.2.1";
            interface = "enp3s0";
        };
        interfaces.enp3s0.ipv4.addresses = [
            {
                address = "192.168.2.5";
                prefixLength = 24;
            }
        ];
        nameservers = [ "1.1.1.1" ];
    };


    documentation = {
        enable = true;
        dev.enable = true;
        doc.enable = true;
        info.enable = true;
        man.enable = true;
    };

    networking.wireless.enable = true;

    i18n = {
        consoleFont = "Lat2-Terminus16";
        consoleKeyMap = "neo";
        defaultLocale = "en_US.UTF-8";
        inputMethod = {
            enabled = "ibus";
            ibus.engines = with pkgs.ibus-engines; [ anthy hangul mozc ];
        };
    };

    time.timeZone = "Etc/GMT-1";

    environment.systemPackages = with pkgs; [
        git wget neovim tmux htop (python37.withPackages(ps: with ps; [ python-uinput ]))
    ];

    services.openssh.enable = true;

    networking.firewall.allowedUDPPorts = [ 8889 ];


    sound.enable = true;
    hardware.pulseaudio.enable = true;

    services.xserver = {
        enable = true;
        layout = "de";
        xkbVariant = "neo";
        libinput.enable = true;
        desktopManager.xterm.enable = false;
        windowManager.i3.enable = true;
        videoDrivers = [ "nvidia" ];
    };
    programs.zsh.enable = true;

    users.users.root = {
        isNormalUser = true;
        isSystemUser = true;
        shell = pkgs.zsh;
        packages = with pkgs; [
            file
            pv
            python
            vim
        ];
    };
    users.users.darkkirb = {
        isNormalUser = true;
        home = "/home/darkkirb";
        extraGroups = [ "wheel" "networkmanager" "adbusers" "docker" ];
        uid = 1000;
        shell = pkgs.zsh;
        packages = with pkgs; [
            anki
            binutils
            blender
            cargo
            cmake
            discord
            dolphinEmuMaster
            ffmpeg-full
            firefox
            gcc-unwrapped
            git
            gnumake
            i3blocks
            libsodium
            lmms
            multimc
            noto-fonts
            openssl
            pkgconfig
            python
            python3
            rofi-unwrapped
            rustc
            rustfmt
            rxvt_unicode_with-plugins
            scrot
            taskwarrior
            texlive.combined.scheme-full
            timewarrior
            (wineStaging.override { wineBuild = "wineWow"; })
            wineStaging
            gnome3.zenity
            zlib
        ];
    };
    programs.adb.enable = true;

# This value determines the NixOS release with which your system is to be
# compatible, in order to avoid breaking some software such as database
# servers. You should change this only after NixOS release notes say you
# should.
    system.stateVersion = "18.09"; # Did you read the comment?

# zfs stuff
    services.zfs = {
        autoScrub.enable = true;
        autoScrub.interval = "weekly";
        autoSnapshot.enable = true;
    };
    services.locate.enable = true;
    services.locate.interval = "daily";
    services.postgresql = {
        enable = true;
        package = pkgs.postgresql100;
        authentication = pkgs.lib.mkOverride 10 ''
            local all all trust
        '';
        initialScript = pkgs.writeText "backend-initScript" ''
            CREATE ROLE poyobot WITH LOGIN PASSWORD 'poyobot' CREATEDB;
            CREATE DATABASE poyobot;
            GRAND ALL PRIVILEGES ON DATABASE poyobot TO poyobot;
        '';
    };
    virtualisation.docker.enable = true;
    nix.nixPath = [ "/etc" "nixos-config=/etc/nixos/configuration.nix" ];
    nixpkgs.config.allowUnfree = true;
}
